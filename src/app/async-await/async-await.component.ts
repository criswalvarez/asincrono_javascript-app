import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-async-await',
  templateUrl: './async-await.component.html',
  styleUrls: ['./async-await.component.css']
})
export class AsyncAwaitComponent implements OnInit {
  respuesta : any
  error : any

  constructor() { }

  ngOnInit() {
  }

  async obtenerUnaImagen(){
    try {
      let enBruto = await fetch('https://randomuser.me/api');
      this.respuesta = await enBruto.json();
    } catch (error) {
      this.error = error
    }

  }
}
