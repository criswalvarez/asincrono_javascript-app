import { Component, OnInit } from '@angular/core';
import { resolve } from 'url';
import { reject } from 'q';
import { strictEqual } from 'assert';

@Component({
  selector: 'app-promise',
  templateUrl: './promise.component.html',
  styleUrls: ['./promise.component.css']
})
export class PromiseComponent implements OnInit {
  promise = new Promise<string>((resolve, reject) => { resolve("Hola");reject("Error") });
  respuesta : any
  constructor() { }

  ngOnInit() {

  }

  obtenerUnaImagen(){
    fetch('https://randomuser.me/api')
      .then(response =>  {
        return response.json();
      })
      .then(json => {
        this.respuesta = json;
      })
      .catch(err => {
        console.error(err);
      });
  }
}
