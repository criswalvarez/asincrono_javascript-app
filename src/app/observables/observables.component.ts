import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-observables',
  templateUrl: './observables.component.html',
  styleUrls: ['./observables.component.css']
})
export class ObservablesComponent implements OnInit {
  conversacionConMama = new Subject<string>()
  manuelito : Observable<string>;
  cris : Observable<string>;
  datos : any
  error : any

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.manuelito = this.conversacionConMama.asObservable();
    this.cris = this.conversacionConMama.asObservable();
  }

  mamaHabla(diceAlgo : string){
    this.conversacionConMama.next(diceAlgo)
  }

  obtenerImagen(){
    this.http.get('https://randomuser.me/api').subscribe(
      ok => {
        this.datos = ok;
      },
      err => {
        this.error = err
      }
    )
  }
}
