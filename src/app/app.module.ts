import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CallbackComponent } from './callback/callback.component';
import { PromiseComponent } from './promise/promise.component';
import { ObservablesComponent } from './observables/observables.component';
//hay que buscalo en internet
//https://angular.io/guide/http
import { HttpClientModule } from '@angular/common/http';
import { AsyncAwaitComponent } from './async-await/async-await.component';


@NgModule({
  declarations: [
    AppComponent,
    CallbackComponent,
    PromiseComponent,
    ObservablesComponent,
    AsyncAwaitComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
